from django.shortcuts import render


def test(request):
    return render(request, 'main/test.html')


def home(request):
    return render(request, 'main/home.html')


def sitesforstudents(request):
    return render(request, 'main/sitesforstudents.html')


def leonardo(request):
    return render(request, 'main/leonardo.html')


def ntu(request):
    return render(request, 'main/ntu.html')
